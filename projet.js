function loadText(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.overrideMimeType("text/plain");
    xhr.send(null);
    if(xhr.status === 200)
        return xhr.responseText;
    else {
        return null;
    }
}

// variables globales du programme;
var canvas;
var gl; //contexte
var program; //shader program

var positionBuffer;
var colorBuffer;

var attribPos; //attribute position
var attribColor;
var uniPers;
var uniTransfo;

var mousePositions = [ ];
var colors = [];

var field  = 45;
var translationValues = {x: 0, y: 0, z: -6.0};
var scaleFactor = 1.0;
var rotationAngle = {x: 0, y: 0, z: 0};

function initContext() {
    canvas = document.getElementById('dawin-webgl');
    gl = canvas.getContext('webgl');
    if (!gl) {
        console.log('ERREUR : echec chargement du contexte');
        return;
    }
    gl.clearColor(0.5, 0.5, 0.5, 1.0);
}

//Initialisation des shaders et du program
function initShaders() {
    var fragmentSource = loadText('fragment.glsl');
    var vertexSource = loadText('vertex.glsl');

    var fragment = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragment, fragmentSource);
    gl.compileShader(fragment);

    var vertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertex, vertexSource);
    gl.compileShader(vertex);

    gl.getShaderParameter(fragment, gl.COMPILE_STATUS);
    gl.getShaderParameter(vertex, gl.COMPILE_STATUS);

    if (!gl.getShaderParameter(fragment, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(fragment));
    }

    if (!gl.getShaderParameter(vertex, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(vertex));
    }

    program = gl.createProgram();
    gl.attachShader(program, fragment);
    gl.attachShader(program, vertex);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.log("Could not initialise shaders");
    }
    gl.useProgram(program);
}

function initEvents() {
    Array.prototype.forEach.call(document.getElementsByClassName("slider"), function(e) {
        switch (e.id) {
            case "tx" : e.oninput = function () {
                translationValues.x = document.getElementById(e.id).value / 100;
            }; break;
            case "ty" : e.oninput = function () {
                translationValues.y = document.getElementById(e.id).value / 100;
            }; break;
            case "tz" : e.oninput = function () {
                translationValues.z = document.getElementById(e.id).value / 100;
            }; break;
            case "rx" : e.oninput = function () {
                rotationAngle.x = document.getElementById(e.id).value * Math.PI / 48;
            }; break;
            case "ry" : e.oninput = function () {
                rotationAngle.y = document.getElementById(e.id).value * Math.PI / 48;
            }; break;
            case "rz" : e.oninput = function () {
                rotationAngle.z = document.getElementById(e.id).value * Math.PI / 48;
            }; break;
            case "zoom" : e.oninput = function () {
                scaleFactor = document.getElementById(e.id).value / 100;
            }; break;
            case "perspective" : e.oninput = function () {
                field = document.getElementById(e.id).value;
            }; break;
            default: break;
        }
    });
}

//Fonction initialisant les attributs pour l'affichage (position et taille)
function initAttributes() {
    attribPos = gl.getAttribLocation(program, "position");
    attribColor = gl.getAttribLocation(program, "color");
    uniTransfo = gl.getUniformLocation(program, "transformation");
    uniPers = gl.getUniformLocation(program, "perspective");
}

//Initialisation des buffers
function initBuffers() {
    colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 12, gl.STREAM_DRAW);
    gl.vertexAttribPointer(1, 4, gl.FLOAT, true, 0, 0);
    gl.enableVertexAttribArray(1);

    positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 9, gl.STREAM_DRAW);
    gl.vertexAttribPointer(attribPos, 3, gl.FLOAT, true, 0, 0);
    gl.enableVertexAttribArray(attribPos);
}

function initPerspective() {
    const fieldOfView = field * Math.PI / 180;
    const aspect = canvas.width / canvas.height;
    const zNear = 0.1;
    const zFar = 100.0;

    var matrix = mat4.create();
    mat4.perspective(matrix, fieldOfView, aspect, zNear, zFar);

    gl.uniformMatrix4fv(uniPers, false, matrix);
}

//Mise a jour des buffers : necessaire car les coordonnees des points sont ajoutees a chaque clic
function refreshBuffers() {
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mousePositions), gl.STATIC_DRAW);
}

//Fonction permettant le dessin dans le canvas
function drawSquare() {
    requestAnimationFrame(drawSquare)

    initPerspective();
    let transformMat = generateTransformMatrix();
    gl.uniformMatrix4fv(uniTransfo, false, transformMat);

    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.drawArrays(gl.TRIANGLES, 0, mousePositions.length / 3);
}

function generateTransformMatrix() {
    let result = mat4.create();
    let rotQuat = quat.create();

    quat.rotateZ(rotQuat, rotQuat, -rotationAngle.z);
    quat.rotateY(rotQuat, rotQuat, -rotationAngle.y);
    quat.rotateX(rotQuat, rotQuat, -rotationAngle.x);

    let translationVec = vec3.fromValues(translationValues.x, translationValues.y, translationValues.z);
    let scaleVec = vec3.fromValues(scaleFactor, scaleFactor, scaleFactor);

    mat4.fromRotationTranslationScale(result, rotQuat, translationVec, scaleVec);
    return result;
}

function setCube() {
    mousePositions.push(...[
        // Front face
        -1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        1.0, -1.0,  1.0,

        -1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,


        // Back face
        -1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
        -1.0,  1.0, -1.0,

        -1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
        1.0, -1.0, -1.0,


        // Top face
        -1.0,  1.0, -1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,

        -1.0,  1.0, -1.0,
        1.0,  1.0,  1.0,
        1.0,  1.0, -1.0,


        // Bottom face
        -1.0, -1.0, -1.0,
        1.0, -1.0,  1.0,
        1.0, -1.0, -1.0,

        -1.0, -1.0, -1.0,
        1.0, -1.0,  1.0,
        -1.0, -1.0,  1.0,


        // Right face
        1.0, -1.0, -1.0,
        1.0,  1.0,  1.0,
        1.0,  1.0, -1.0,

        1.0, -1.0, -1.0,
        1.0,  1.0,  1.0,
        1.0, -1.0,  1.0,


        // Left face
        -1.0, -1.0, -1.0,
        -1.0,  1.0,  1.0,
        -1.0, -1.0,  1.0,

        -1.0, -1.0, -1.0,
        -1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0
    ]);

    colors.push(...([
        Array(6).fill([1.0, 0.0, 0.0, 1.0]).flat(),
        Array(6).fill([0.0, 1.0, 0.0, 1.0]).flat(),
        Array(6).fill([0.0, 0.0, 1.0, 1.0]).flat(),
        Array(6).fill([1.0, 1.0, 0.0, 1.0]).flat(),
        Array(6).fill([0.0, 1.0, 1.0, 1.0]).flat(),
        Array(6).fill([1.0, 0.0, 1.0, 1.0]).flat()
    ].flat()));

    refreshBuffers();
}

function main() {
    initContext();
    initShaders();
    initAttributes();
    initPerspective();
    initBuffers();
    initEvents();

    setCube();
    drawSquare();
}
